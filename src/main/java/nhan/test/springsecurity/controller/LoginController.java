package nhan.test.springsecurity.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import nhan.test.springsecurity.security.JwtTokenUtils;
import nhan.test.springsecurity.user.UserAuthentication;
import nhan.test.springsecurity.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = LoginController.LOGIN_CONTROLLER)
public class LoginController {
	final static String LOGIN_CONTROLLER = "/login";

	private final UserService userService;
	private final JwtTokenUtils tokenUtils;

	@Autowired
	public LoginController(UserService userService, JwtTokenUtils tokenUtils) {
		this.userService = userService;
		this.tokenUtils = tokenUtils;
	}

	@PostMapping
	public ResponseEntity<JwtResponse> login(@RequestBody JwtRequest request) {
		UserAuthentication user = userService.loadUserByUserName(request.getUserName());
		if (user != null) {
			String token = tokenUtils.generateToken(user);
			return new ResponseEntity<>(new JwtResponse(token), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}

@AllArgsConstructor
@Data
final class JwtRequest {
	private String userName;
	private String password;
}

@AllArgsConstructor
@Data
final class JwtResponse {
	String token;
}
