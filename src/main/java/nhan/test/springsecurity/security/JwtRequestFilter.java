package nhan.test.springsecurity.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.ExpiredJwtException;
import nhan.test.springsecurity.user.UserAuthentication;
import nhan.test.springsecurity.user.UserService;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

	private final JwtTokenUtils jwtUtils;
	private final UserService userService;

	@Autowired
	public JwtRequestFilter(JwtTokenUtils jwtUtils, UserService userService) {
		this.jwtUtils = jwtUtils;
		this.userService = userService;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		final String requestTokenHeader = request.getHeader("x-token");
		String jwtToken = null;
		String userName = null;
		if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
			jwtToken = requestTokenHeader.substring(7);
			try {
				userName = jwtUtils.getUserNameFromToken(jwtToken);
			} catch (IllegalArgumentException e) {
				System.out.println("Unable to get JWT Token");
			} catch (ExpiredJwtException e) {
				System.out.println("JWT Token has expired");
			}
		} else {
			logger.warn("JWT Token does not begin with Bearer String");
		}

		if (userName != null && SecurityContextHolder.getContext().getAuthentication() == null) {
			UserAuthentication userDetails = userService.loadUserByUserName(userName);
			// if token is valid configure Spring Security to manually set
			// authentication
			if (jwtUtils.isTokenValid(jwtToken, userDetails)) {
				// After setting the Authentication in the context, we specify
				// that the current user is authenticated. So it passes the
				// Spring Security Configurations successfully.
				SecurityContextHolder.getContext().setAuthentication(userDetails);
			}
		}
		filterChain.doFilter(request, response);
	}

}
