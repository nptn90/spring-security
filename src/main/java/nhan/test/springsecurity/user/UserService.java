package nhan.test.springsecurity.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
	private final UserRepository userRepo;
	@Autowired
	public UserService(UserRepository userRepo) {
		this.userRepo = userRepo;
	}

	public UserAuthentication loadUserByUserName(String name) {
		return userRepo.loadUserByUserName(name);
	}

}
