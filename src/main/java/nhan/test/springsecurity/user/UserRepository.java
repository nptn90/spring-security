package nhan.test.springsecurity.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {

	@Value(value = "${system.user.username}")
	private String userName;
	
	@Value(value = "${system.user.password}")
	private String password;
	
	@Value(value = "${system.user.roles}")
	private List<String> roles;
	
	public UserAuthentication loadUserByUserName(String name) {
		UserAuthentication userDB = new UserAuthentication(userName, password, roles);
		if (name.equals(userName)) {
			return userDB;
		}
		return null;
	}
}
